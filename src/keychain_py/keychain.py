#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This is the implementation of keychain-py.

It is a toy prototype do not use this.

"""
from __future__ import division, print_function, absolute_import

import argparse
import sys
import logging
import pickle
from getpass import getpass

import nacl
import nacl.hash
import nacl.utils
import nacl.secret
from nacl.bindings.sodium_core import sodium_init

from keychain_py import __version__

__author__ = "Derek Goddeau"
__copyright__ = "Derek Goddeau"
__license__ = "mit"

_logger = logging.getLogger(__name__)


class Keychain():
    """
    This is insecure due to the lack of the correct algorithms being
    available in any Python libsodium bindings.

    """
    def __init__(self, name, password_hash):
        sodium_init()
        self.name = name
        self.password_hash = password_hash
        self.domain_hasher = nacl.hash.sha256
        self.domain_hash_encoding = nacl.encoding.HexEncoder
        self.kdf = nacl.pwhash.argon2id.kdf
        self.opslimit = nacl.pwhash.argon2id.OPSLIMIT_INTERACTIVE
        self.memlimit = nacl.pwhash.argon2id.MEMLIMIT_INTERACTIVE
        self.nonce = nacl.utils.random(nacl.secret.SecretBox.NONCE_SIZE)
        self.keychain = {}
    
    def add_domain(self, domain, password):
        """Add a domain.

        Yes, the nonce is too small, this is a toy.

        """
        if not nacl.pwhash.verify(self.password_hash, password):
            _logger.error("Incorrect password")
            return False

        domain_hash = self.domain_hasher(domain.encode(), encoder=self.domain_hash_encoding)
        if domain_hash in self.keychain:
            _logger.error("Domain already exists")
            return False

        domain_salt = nacl.utils.random(nacl.pwhash.argon2i.SALTBYTES)
        domain_key = self.kdf(nacl.secret.SecretBox.KEY_SIZE,
                              password,
                              domain_salt,
                              opslimit=self.opslimit,
                              memlimit=self.memlimit)

        domain_box = nacl.secret.SecretBox(domain_key)
        encrypted_vault = domain_box.encrypt(pickle.dumps({
            'domain': domain,
            'password': getpass("Enter password for {}: ".format(domain))
            }),
            self.nonce
        )

        self.keychain[domain_hash] = {
            'vault': encrypted_vault,
            'nonce': self.nonce,
            'salt': domain_salt
        }
        self.nonce = nacl.utils.random(nacl.secret.SecretBox.NONCE_SIZE)

    def get_domain(self, domain, password):
        """Get a domains password.

        Don't print the vault to std out this is a toy.

        """
        if not nacl.pwhash.verify(self.password_hash, password):
            _logger.error("Incorrect password")
            return False

        domain_hash = self.domain_hasher(domain.encode(), encoder=self.domain_hash_encoding)
        if domain_hash not in self.keychain:
            _logger.error("No entry for domain")
            return False

        domain_salt = self.keychain[domain_hash]['salt']
        domain_key = self.kdf(nacl.secret.SecretBox.KEY_SIZE,
                              password,
                              domain_salt,
                              opslimit=self.opslimit,
                              memlimit=self.memlimit)

        domain_nonce = self.keychain[domain_hash]['nonce']
        domain_box = nacl.secret.SecretBox(domain_key)
        decrypted = domain_box.decrypt(self.keychain[domain_hash]['vault'])
        print(pickle.loads(decrypted))
        

    def remove_domain(self, domain, password):
        """Remove a domain.

        """
        if not nacl.pwhash.verify(self.password_hash, password):
            _logger.error("Incorrect password")
            return False

        domain_hash = self.domain_hasher(domain.encode(), encoder=self.domain_hash_encoding)
        if domain_hash not in self.keychain:
            _logger.error("No entry for domain")
            return False

        del self.keychain[domain_hash]
