#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This is the CLI interface for keychain-py.
"""
from __future__ import division, print_function, absolute_import

import os
import argparse
import sys
import logging
import pickle
import yaml
from getpass import getpass

from nacl import pwhash
from appdirs import user_data_dir, user_config_dir

from keychain_py import __version__
from keychain_py.keychain import Keychain

__author__ = "Derek Goddeau"
__copyright__ = "Derek Goddeau"
__license__ = "mit"

_logger = logging.getLogger(__name__)


def parse_args(args):
    """Parse command line parameters

    Args:
      args ([str]): command line parameters as list of strings

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace

    """
    parser = argparse.ArgumentParser(
        description="Just a Fibonnaci demonstration")
    parser.add_argument(
        '--version',
        action='version',
        version='keychain-py {ver}'.format(ver=__version__))
    parser.add_argument(
        '-v',
        '--verbose',
        dest="loglevel",
        help="set loglevel to INFO",
        action='store_const',
        const=logging.INFO)
    parser.add_argument(
        '-vv',
        '--very-verbose',
        dest="loglevel",
        help="set loglevel to DEBUG",
        action='store_const',
        const=logging.DEBUG)
    parser.add_argument(
        '-n',
        '--new',
        dest="new",
        help="Create a new Keychain",
        type=str,
        metavar="STR")
    parser.add_argument(
        '-o',
        '--open',
        dest="keyfile",
        help="Open an existing Keychain by name",
        type=str,
        metavar="STR")
    parser.add_argument(
        '-a',
        '--add',
        dest="add",
        help="Add a new entry",
        type=str,
        metavar="STR")
    parser.add_argument(
        '-g',
        '--get',
        dest="get",
        help="Get an entry",
        type=str,
        metavar="STR")
    parser.add_argument(
        '-d',
        '--delete',
        dest="delete",
        help="Delete an existing entry",
        type=str,
        metavar="STR")
    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages

    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(level=loglevel, stream=sys.stdout,
                        format=logformat, datefmt="%Y-%m-%d %H:%M:%S")


def main(args):
    """Main entry point allowing external calls

    Args:
      args ([str]): command line parameter list
    """
    args = parse_args(args)
    setup_logging(args.loglevel)
    _logger.debug("Starting keychain operations...")

    if args.new:
        _logger.info("Creating new keychain: {}".format(args.new))
        keychain = Keychain(args.new, pwhash.str(getpass().encode()))
        save_keychain(keychain)
        set_keyfile(args.new)
    elif args.keyfile:
        file_dir = user_config_dir("keychain-py", __author__)
        file_path = file_dir + '/' + args.keyfile + '.pkl'
        set_keyfile(args.keyfile)
    elif args.add:
        _logger.info("Adding keychain entry: {}".format(args.add))
        keychain = open_keychain()
        keychain.add_domain(args.add, getpass().encode())
        save_keychain(keychain)
    elif args.get:
        _logger.info("Fetching keychain entry: {}".format(args.get))
        keychain = open_keychain()
        keychain.get_domain(args.get, getpass().encode())
    elif args.delete:
        _logger.info("Deleting keychain entry: {}".format(args.delete))
        keychain = open_keychain()
        keychain.remove_domain(args.delete, getpass().encode())
        save_keychain(keychain)

    _logger.debug("Exiting")


def open_keychain():
    current_keychain = ''
    config_dir = user_config_dir("keychain-py", __author__)
    config_file_path = config_dir + '/config.yml'
    with open(config_file_path, 'r') as config_input_file:
        current_keychain = yaml.load(config_input_file)['current_keychain']
    keychain_dir = user_data_dir("keychain-py", __author__)
    keychain_file_path = keychain_dir + '/' + current_keychain + '.pkl'
    with open(keychain_file_path, 'rb') as keychain_input_file:
        return pickle.load(keychain_input_file)


def save_keychain(keychain):
    file_dir = user_data_dir("keychain-py", __author__)
    file_path = file_dir + '/' + keychain.name + '.pkl'
    os.makedirs(file_dir, exist_ok=True)
    with open(file_path, 'wb') as output_file:
        pickle.dump(keychain, output_file)
        _logger.debug("Saved keychain to: {}".format(output_file))


def set_keyfile(name):
    _logger.info("Opening keychain from: {}".format(name))
    file_dir = user_config_dir("keychain-py", __author__)
    file_path = file_dir + '/config.yml'
    os.makedirs(file_dir, exist_ok=True)
    with open(file_path, 'w') as output_file:
        config = { 'current_keychain': name }
        yaml.dump(config, output_file)
        _logger.debug("Saved config to: {}".format(output_file))


def run():
    """Entry point for console_scripts

    """
    main(sys.argv[1:])


if __name__ == "__main__":
    run()
