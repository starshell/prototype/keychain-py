===========
keychain-py
===========

A prototype keychain for fleshing out high level design using
libsodium like bindings for various projects.
This is being made for prototyping API, data structures, etc.
before writing in another language. See notes below for reasons
why Python should never be used to handle sensitive data.

After a survey of all available python packages for libsodium
not a single one is as viable as the Rust package ``sodiumoxide``
even disreguarding the issues with python.

Design:
=======

To not leak any information about the domains hashes are used to look up a
domains entry. This prevents leaking of the length of the domain entry name.

.. code-block::

    passwords = {
        'hash_of_domain': { 
            'enctypted': {
                'domain_name': 'Proton Mail',
                'password': super_secret,
                'nonce': unique_number
            },
            'additional_data': some_salt
        },
        'additional_data': some_salt
    }

Notes:
======

Decrypting and encrypting in Python will not work. Strings and Integers are
interned and persistent, which means you are leaving a mess of password information
all over the place. If you ever read the password into a Python object, there is
the potential for it being compromised by a dump. Hashing is the standard answer,
though of course the plaintext eventually needs to be processed somewhere. Python
doesn't have enough low level control over memory. The best you can do is to
``del password`` after use so that no references to the password string object
remain. Any solution that purports to be able to do more than that is only giving
you a false sense of security. Python string objects are immutable; there's no
direct way to change the contents of a string after it is created. Even if you
were able to somehow overwrite the contents of the string referred to by password
(which is possible with ctypes tricks [#ctype]_), there could still be other
copies of the password that have been created in various string operations:

- The ``getpass`` module when it strips the trailing newline off of the password input
- by the ``imaplib`` module when it quotes the password and then creates the complete IMAP command before passing it off to the socket

You would somehow have to get references to all of those strings and overwrite their memory as well.

The correct solution is to do the sensitive processes in another language.

.. [#ctype] `Clearing Passwords in Memory with Python <http://web.archive.org/web/20100929111257/http://www.codexon.com/posts/clearing-passwords-in-memory-with-python>`_

--------------------------------------------------------------------------------

Maybe a custom `TextInput <https://github.com/kivy/kivy/blob/master/kivy/uix/textinput.py/>`_ for kivy would would work as well as below.

Alternatives
============

Do not look any better with garbage collection, etc. also.


Swift
-----

`Secure Memory For Swift Objects <https://stackoverflow.com/questions/27715985/secure-memory-for-swift-objects?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa>`_

Java
----

`Why is char[] prefered over String for passwords? <https://stackoverflow.com/questions/8881291/why-is-char-preferred-over-string-for-passwords?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa>`_
